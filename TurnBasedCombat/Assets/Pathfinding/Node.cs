﻿using UnityEngine;
using System.Collections.Generic;

namespace TurnBaseCombat.Assets.Pathfinding {
    public class Node {
        public List<Node> neighbours;
        public TileCoordinates tileCoordinates;

        public int x
        {
            get { return tileCoordinates.x; }
        }

        public int z
        {
            get { return tileCoordinates.z; }
        }

        public Node() {
            neighbours = new List<Node>();
        }

        public Node(int x, int z) : this()
        {
            tileCoordinates = new TileCoordinates(x, z);
        }

        public float DistanceTo(Node n) {
            if (n == null) {
                Debug.LogError("WTF?");
            }

            return Vector2.Distance(
                new Vector2(x, z),
                new Vector2(n.x, n.z)
                );
        }
    }	
}
