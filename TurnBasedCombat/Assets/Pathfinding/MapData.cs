﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TurnBaseCombat.Assets.Pathfinding
{
    [CreateAssetMenu(fileName = "MapData", menuName = "MapData", order = 1)]
    public class MapData : ScriptableObject
    {
        public int mapSizeX = 10;
        public int mapSizeZ = 10;

        [SerializeField] int[] tilesSerialized;

        public float tileSize;

        public void SerializeMap(int[,] tiles)
        {
            tilesSerialized = new int[mapSizeX * mapSizeZ];

            for (int x = 0; x < mapSizeX; x++)
            {
                for (int z = 0; z < mapSizeZ; z++)
                {
                    //tiles[x, y] = 0;
                    //index = y * gridWidth + x;
                    var index = z * mapSizeX + x;
                    tilesSerialized[index] = tiles[x, z];
                }
            }
#if Unity_Editor
            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }

        public int[,] DeserializeMap()
        {
            var tiles = new int[mapSizeX, mapSizeZ];

            var length = mapSizeX * mapSizeZ;
            for (int i = 0; i < length; i++)
            {
                int z = (int)(i / (float)mapSizeX);
                int x = i - (z * mapSizeX);
                tiles[x, z] = tilesSerialized[i];
            }

            return tiles;
        }


        [ContextMenu("Create empty Map")]
        private void CreateNewMapData()
        {
            var length = mapSizeX * mapSizeZ;
            tilesSerialized = new int[length];
            for (int i = 0; i < length; i++)
            {
                tilesSerialized[i] = 0;
            }
        }

    }
}
