using UnityEngine;
using TurnBaseCombat.Assets.Code.AllManagers;
using TurnBaseCombat.Assets.Code.Units;

namespace TurnBaseCombat.Assets.Pathfinding {
    [SelectionBase]
    public class ClickableTile : MonoBehaviour {

        public int tileX;
        public int tileZ;

        TileMap map
        {
            get { return Managers.Game.map; }
        }
        Unit selectedUnit
        {
            get { return Managers.Turn.SelectedUnit; }
        }

        public void Init(int x, int z, int type) {
            tileX = x;
            tileZ = z;

            switch (type)
            {
                case 0:
                case 1:
                    ChangeMaterialToAvailable();
                    break;
                case 2:
                    ChangeMaterialToUnavailable();
                    break;
                default:
                    break;
            }


        }

        internal void AimTile()
        {
            if (selectedUnit != null)
                selectedUnit.CurrentAction.OnAimTile(this);
        }

        /// DOIT refactor name
        internal void UnAim()
        {
            
        }

        public void OnClick() {
            if (selectedUnit != null)
                selectedUnit.CurrentAction.ClickTile(this);
        }

        [ContextMenu("Change to Grass")]
        void ChangeToGrass()
        {
            map.ChangeTileType(tileX, tileZ, 0);
            ChangeMaterialToAvailable();
        }

        [ContextMenu("Change to Mountain")]
        void ChangeToMountain()
        {
            map.ChangeTileType(tileX, tileZ, 2);
            ChangeMaterialToUnavailable();
        }

        void ChangeMaterial(string materialPath)
        {
            var mat = Resources.Load(materialPath, typeof(Material)) as Material;
            GetComponentInChildren<Renderer>().material = mat;
        }

        public void ChangeMaterialToUnavailable()
        {
            ChangeMaterial(@"Materials\Tile\Tile_unavailable");
        }

        public void ChangeMaterialToAvailable()
        {
            ChangeMaterial(@"Materials\Tile\Tile_available");
        }

        public void ChangeMaterialToSelected()
        {
            ChangeMaterial(@"Materials\Tile\Tile_Selected");
        }
    }
}
