﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using TurnBaseCombat.Assets.Code.AllManagers;
using TurnBaseCombat.Assets.Code.Units;

namespace TurnBaseCombat.Assets.Pathfinding {
    public class TileMap : MonoBehaviour {
        
        public TileType[] tileTypes;
        
        int[,] tiles;
        Node[,] graph;

        [SerializeField] MapData mapData;

        [SerializeField] ClickableTile[,] clickableTiles;

        public int[,] Tiles
        {
            get
            {
                if (tiles == null)
                    DeserializeMap();
                return tiles;
            }

            set
            {
                tiles = value;
#if UNITY_EDITOR
                if (Application.isPlaying == false)
                {
                    UnityEditor.Undo.RecordObject(this, "change tiles");
                    SerializeMap();
                }
#endif
            }
        }

        void Awake()
        {
            DeserializeMap();

            if (transform.childCount <= 0)
            {
                Debug.LogWarning("No map found. Generating");
                GenerateMapVisual();
            }

            GeneratePathfindingGraph();
        }

        public void Start()
        {
            HideSpots();
        }

        public void HideSpots()
        {
            foreach (var t in clickableTiles)
            {
                t.gameObject.SetActive(false);
            }
        }

        internal void SetAvailableSpots(List<Node> spots)
        {
            HideSpots();

            foreach (var s in spots)
            {
                var tile = clickableTiles[s.x, s.z];
                tile.gameObject.SetActive(true);
                tile.ChangeMaterialToAvailable();
            }
        }

        internal void ShowSpotSelected(TileCoordinates coordinates)
        {
            var tile = clickableTiles[coordinates.x, coordinates.z];
            tile.gameObject.SetActive(true);
            tile.ChangeMaterialToSelected();
        }

        internal void ShowSpotTarget(TileCoordinates coordinates)
        {
            var tile = clickableTiles[coordinates.x, coordinates.z];
            tile.gameObject.SetActive(true);
            tile.ChangeMaterialToUnavailable();
        }

        [ContextMenu("serialize map")]
        void SerializeMap()
        {
            mapData.SerializeMap(Tiles);
        }

        [ContextMenu("deserialize map")]
        void DeserializeMap()
        {
            tiles = mapData.DeserializeMap();
        }

        

        internal void ChangeTileType(int tileX, int tileY, int v)
        {
#if UNITY_EDITOR
            if (Application.isPlaying == false)
                UnityEditor.Undo.RecordObject(this, "Change map");
#endif
            Tiles[tileX, tileY] = v;
            SerializeMap();
        }

        [ContextMenu("Create Map")]
        void CreateMap()
        {
            DeserializeMap();
            GenerateMapVisual();
        }

        [ContextMenu("Clear Map")]
        void ClearMap()
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                var child = transform.GetChild(i);
                if (Application.isPlaying)
                    Destroy(child.gameObject);
#if UNITY_EDITOR
                else
                    UnityEditor.Undo.DestroyObjectImmediate(child.gameObject);
#endif
            }
        }
        
        [ContextMenu("Generate map visual")]
        void GenerateMapVisual() {
            clickableTiles = new ClickableTile[mapData.mapSizeX, mapData.mapSizeZ];

            for (int x = 0; x < mapData.mapSizeX; x++) {
                for (int z = 0; z < mapData.mapSizeZ; z++) {
                    TileType tt = tileTypes[Tiles[x, z]];
                    Vector3 position = TileCoordToWorldCoord(x, z);
                    GameObject go = (GameObject)Instantiate(tt.tileVisualPrefab, position, Quaternion.identity);
                    go.transform.localScale = new Vector3(mapData.tileSize, 1, mapData.tileSize);
                    go.transform.SetParent(transform, true);

                    ClickableTile ct = go.GetComponent<ClickableTile>();
                    ct.Init(x, z, Tiles[x, z]);

                    clickableTiles[x, z] = ct;
                }
            }
        }

        public float CostToEnterTile(int sourceX, int sourceZ, int targetX, int targetZ) {

            TileType tt = tileTypes[Tiles[targetX, targetZ]];

            if (UnitCanEnterTile(targetX, targetZ) == false)
                return Mathf.Infinity;

            float cost = tt.movementCost;

            if (sourceX != targetX && sourceZ != targetZ)
            {
                // We are moving diagonally!  Fudge the cost for tie-breaking
                // Purely a cosmetic thing!
                cost += 0.001f;
            }

            return cost;

        }

        void GeneratePathfindingGraph() {
            // Initialize the array
            graph = new Node[mapData.mapSizeX, mapData.mapSizeZ];

            // Initialize a Node for each spot in the array
            for (int x = 0; x < mapData.mapSizeX; x++) {
                for (int z = 0; z < mapData.mapSizeZ; z++) {
                    graph[x, z] = new Node(x, z);
                }
            }

            // Now that all the nodes exist, calculate their neighbours
            for (int x = 0; x < mapData.mapSizeX; x++) {
                for (int z = 0; z < mapData.mapSizeZ; z++) {
                    // This is the 8-way connection version (allows diagonal movement)
                    // Try left
                    if (x > 0) {
                        graph[x, z].neighbours.Add(graph[x - 1, z]);
                        if (z > 0)
                            graph[x, z].neighbours.Add(graph[x - 1, z - 1]);
                        if (z < mapData.mapSizeZ - 1)
                            graph[x, z].neighbours.Add(graph[x - 1, z + 1]);
                    }

                    // Try Right
                    if (x < mapData.mapSizeX - 1) {
                        graph[x, z].neighbours.Add(graph[x + 1, z]);
                        if (z > 0)
                            graph[x, z].neighbours.Add(graph[x + 1, z - 1]);
                        if (z < mapData.mapSizeZ - 1)
                            graph[x, z].neighbours.Add(graph[x + 1, z + 1]);
                    }

                    // Try straight up and down
                    if (z > 0)
                        graph[x, z].neighbours.Add(graph[x, z - 1]);
                    if (z < mapData.mapSizeZ - 1)
                        graph[x, z].neighbours.Add(graph[x, z + 1]);
                }
            }
        }

        public Vector3 TileCoordToWorldCoord(int x, int z)
        {
            return transform.position + new Vector3(x * mapData.tileSize, 0, z * mapData.tileSize);
        }

        public Vector3 TileCoordToWorldCoord(TileCoordinates tileCoordinates) {
            return TileCoordToWorldCoord(tileCoordinates.x, tileCoordinates.z);
        }

        public TileCoordinates WorldPositionToTileCoord(Vector3 worldCoord)
        {
            var position = worldCoord - transform.position;
            int x = (int)(position.x / mapData.tileSize);
            int z = (int)(position.z / mapData.tileSize);
            return new TileCoordinates(x, z);
        }

        public bool UnitCanEnterTile(int x, int z) {

            // We could test the unit's walk/hover/fly type against various
            // terrain flags here to see if they are allowed to enter the tile.

            return tileTypes[Tiles[x, z]].isWalkable && !OtherUnitOnThatTile(x, z);
        }

        private bool OtherUnitOnThatTile(int x, int z)
        {
            var allUnits = Managers.Turn.GetAllUnits();
            foreach (var unit in allUnits)
            {
                if (unit.tileCoordinates.x == x && unit.tileCoordinates.z == z)
                    return true;
            }

            return false;
        }

        public List<Node> GeneratePathTo(Unit unit, int x, int z) {
            
            if (UnitCanEnterTile(x, z) == false) {
                // We probably clicked on a mountain or something, so just quit out.
                return null;
            }

            Dictionary<Node, float> dist = new Dictionary<Node, float>();
            Dictionary<Node, Node> prev = new Dictionary<Node, Node>();

            // Setup the "Q" -- the list of nodes we haven't checked yet.
            List<Node> unvisited = new List<Node>();

            Node source = graph[unit.tileCoordinates.x, unit.tileCoordinates.z];

            Node target = graph[x, z];

            dist[source] = 0;
            prev[source] = null;

            // Initialize everything to have INFINITY distance, since
            // we don't know any better right now. Also, it's possible
            // that some nodes CAN'T be reached from the source,
            // which would make INFINITY a reasonable value
            foreach (Node v in graph) {
                if (v != source) {
                    dist[v] = Mathf.Infinity;
                    prev[v] = null;
                }

                unvisited.Add(v);
            }

            while (unvisited.Count > 0) {
                // "u" is going to be the unvisited node with the smallest distance.
                Node u = null;

                foreach (Node possibleU in unvisited) {
                    if (u == null || dist[possibleU] < dist[u]) {
                        u = possibleU;
                    }
                }

                if (u == target) {
                    break;  // Exit the while loop!
                }

                unvisited.Remove(u);

                foreach (Node v in u.neighbours) {
                    //float alt = dist[u] + u.DistanceTo(v);
                    float alt = dist[u] + CostToEnterTile(u.x, u.z, v.x, v.z);
                    if (alt < dist[v]) {
                        dist[v] = alt;
                        prev[v] = u;
                    }
                }
            }

            // If we get there, the either we found the shortest route
            // to our target, or there is no route at ALL to our target.

            if (prev[target] == null) {
                // No route between our target and the source
                return null;
            }

            List<Node> currentPath = new List<Node>();

            Node curr = target;

            // Step through the "prev" chain and add it to our path
            while (curr != null) {
                currentPath.Add(curr);
                curr = prev[curr];
            }

            // Right now, currentPath describes a route from out target to our source
            // So we need to invert it!

            currentPath.Reverse();

            return currentPath;
        }

        public List<Node> FindAvailableSpots(Unit unit, float distance)
        {
            List<Node> availableSpots = new List<Node>();

            Dictionary<Node, float> dist = new Dictionary<Node, float>();
            Dictionary<Node, Node> prev = new Dictionary<Node, Node>();

            // Setup the "Q" -- the list of nodes we haven't checked yet.
            List<Node> unvisited = new List<Node>();

            Node source = graph[unit.tileCoordinates.x, unit.tileCoordinates.z];

            dist[source] = 0;
            prev[source] = null;

            // Initialize everything to have INFINITY distance, since
            // we don't know any better right now. Also, it's possible
            // that some nodes CAN'T be reached from the source,
            // which would make INFINITY a reasonable value
            foreach (Node v in graph)
            {
                if (v != source)
                {
                    dist[v] = Mathf.Infinity;
                    prev[v] = null;
                }

                unvisited.Add(v);
            }

            while (unvisited.Count > 0)
            {
                // "u" is going to be the unvisited node with the smallest distance.
                Node u = null;

                foreach (Node possibleU in unvisited)
                {
                    if (u == null || dist[possibleU] < dist[u])
                    {
                        u = possibleU;
                    }
                }

                unvisited.Remove(u);

                foreach (Node v in u.neighbours)
                {
                    //float alt = dist[u] + u.DistanceTo(v);
                    float alt = dist[u] + CostToEnterTile(u.x, u.z, v.x, v.z);
                    if (alt < dist[v])
                    {
                        dist[v] = alt;
                        prev[v] = u;
                    }
                }
            }
            
            foreach (Node node in graph)
            {
                if (dist[node] <= distance + .01f && node != source)
                    availableSpots.Add(node);
            }
            distances = dist;

            return availableSpots;
        }

        Dictionary<Node, float> distances = new Dictionary<Node, float>();

        private void OnDrawGizmos()
        {
            foreach (var d in distances)
            {
                var position = TileCoordToWorldCoord(d.Key.x, d.Key.z);
#if Unity_Editor
                UnityEditor.Handles.Label(position, d.Value.ToString());
#endif
            }

            for (int x = 0; x < mapData.mapSizeX; x++)
            {
                for (int z = 0; z < mapData.mapSizeZ; z++)
                {
                    var type = Tiles[x, z];
                    var position = TileCoordToWorldCoord(x, z);
                    Color c = Color.green;
                    if (type == 1)
                        c = Color.blue;
                    else if (type == 2)
                        c = Color.gray;

                    c.a = .25f;
                    Gizmos.color = c;
                    Gizmos.DrawCube(position, new Vector3(mapData.tileSize * .9f, .1f, mapData.tileSize * .9f));
                }
            }
        }
    }
}
