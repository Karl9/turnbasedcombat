﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurnBaseCombat.Assets.Pathfinding
{
    public struct TileCoordinates
    {
        public int x;
        public int z;

        public TileCoordinates(int x, int z)
        {
            this.x = x;
            this.z = z;
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", x, z);
        }
    }
}
