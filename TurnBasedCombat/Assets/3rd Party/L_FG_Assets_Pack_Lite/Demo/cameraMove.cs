﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMove : MonoBehaviour {

    private float rSpeed = 3.0f;
    private float mSpeed = 20.0f;
    private float X = 0.0f;
    private float Y = 0.0f;

    public bool rotation = true;
    public bool movement = true;
    public bool zoom = true;

    void Update()
    {

        if (rotation || Input.GetMouseButton(1))
        {
            X += Input.GetAxis("Mouse X") * rSpeed;
            Y += Input.GetAxis("Mouse Y") * rSpeed;
            transform.localRotation = Quaternion.AngleAxis(X, Vector3.up);
            transform.localRotation *= Quaternion.AngleAxis(Y, Vector3.left);
        }

        if (movement)
        {
            var euler = transform.rotation.eulerAngles;
            euler.x = euler.z = 0;
            var rotation = Quaternion.Euler(euler);

            transform.position += rotation * Vector3.forward * mSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
            transform.position += rotation * Vector3.right * mSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
        }

        if (zoom)
        {
            transform.position += transform.forward * mSpeed * Input.mouseScrollDelta.y * Time.deltaTime;
            // clamp y (y, 2, 20)
        }
    }
}
