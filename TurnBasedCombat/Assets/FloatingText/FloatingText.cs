﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour {
    public Animator animator;
    private Text damageText;

    Vector3 worldPosition;

    public Text DamageText
    {
        get
        {
            if (damageText == null)
                damageText = animator.GetComponent<Text>();
            return damageText;
        }
    }

    public void SetText(Vector3 position, string text, Color color)
    {
        worldPosition = position;
        DamageText.text = text;
        DamageText.color = color;

        Destroy(gameObject, 5f);
    }

    private void Update()
    {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(worldPosition);
        transform.position = screenPosition;
    }
}
