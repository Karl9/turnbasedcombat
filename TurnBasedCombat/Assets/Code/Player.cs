﻿using System.Collections.Generic;
using System.Linq;
using TurnBaseCombat.Assets.Code.Units;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code {
    public class Player : MonoBehaviour {
        public string playerName;
        public Color color;

        public List<Unit> units;

        private void Awake()
        {
            units = GetComponentsInChildren<Unit>().ToList();
            foreach (var u in units)
            {
                u.Init(this);
            }
        }
    }
}
