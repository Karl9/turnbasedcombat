﻿using TurnBaseCombat.Assets.Code.Units;
using TurnBaseCombat.Assets.Pathfinding;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.AllManagers {
    [RequireComponent(typeof(Camera))]
    public class GridRaycast : MonoBehaviour {
        new Camera camera;

        [SerializeField] LayerMask rayMask;

#pragma warning disable 414
        /// Just for identifying 
        [SerializeField] Collider hitedObject = null;
#pragma warning restore 414

        ClickableTile aimedTile;
        public ClickableTile AimedTile
        {
            get
            {
                return aimedTile;
            }
            set
            {
                if (value == aimedTile)
                    return;

                if (aimedTile != null)
                    aimedTile.UnAim();

                aimedTile = value;

                if (aimedTile != null)
                    aimedTile.AimTile();
            }
        }

        private void Awake()
        {
            camera = GetComponent<Camera>();
        }

        void Update () {
            DoRaycast();

            if (AimedTile != null)
            {
                if (Input.GetMouseButtonUp(0))
                {
                    if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                        return;

                    AimedTile.OnClick();
                }
            }

        }

        private void DoRaycast() {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 30f, rayMask)) {
                Managers.Game.aimedPoint = hit.point;

                hitedObject = hit.collider;
                AimedTile = hit.collider.GetComponent<ClickableTile>();

                if (Input.GetMouseButtonUp(0))
                {
                    if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                        return;

                    var unit = hit.collider.GetComponent<Unit>();
                    if (unit != null)
                    {
                        unit.ClickUnit();
                    }
                }
            }
            else
            {
                Managers.Game.aimedPoint = null;
                hitedObject = null;
                AimedTile = null;
            }
        }

        
    }
}
