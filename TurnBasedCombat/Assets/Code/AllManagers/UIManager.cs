﻿using UnityEngine;
using TurnBaseCombat.Assets.Code.UI;
using System.Collections.Generic;
using TurnBaseCombat.Assets.Code.UI.Bars;
using TurnBaseCombat.Assets.Code.UI.Buttons;
using TurnBaseCombat.Assets.Code.Units;
using TurnBaseCombat.Assets.Code.Units.UnitActions;

namespace TurnBaseCombat.Assets.Code.AllManagers {
    public class UIManager : MonoBehaviour {
        public InGameUI inGameUI;

        public EnergyBar energyBar;

        public HealthBar healthBarPrefab;
        public Transform healthBarsParent;

        public ActionButton actionButtonPrefab;
        public Transform actionButtonParent;

        public UnitButton unitButtonPrefab;
        public Transform unitsBar1;
        public Transform unitsBar2;

        public FloatingText floatingTextPrefab;
        public Transform floatingTextsParent;


        public GameObject endGame;
        public UnityEngine.UI.Text endGameText;

        public void InitHealthBar(UnitHealth unitHealth, int maxHealth, Color color)
        {
            HealthBar healthBar = Instantiate(healthBarPrefab);
            healthBar.transform.SetParent(healthBarsParent);
            healthBar.Init(unitHealth, maxHealth, color);
        }

        internal void ShowEnergy(int energy, int maxEnergy, Color unitColor)
        {
            energyBar.UpdateEnergyBar(energy, maxEnergy, unitColor);
        }

        internal void InitActionsBar(Unit unit, List<UnitAction> actions, Color color)
        {
            actionButtonParent.DestroyChildren();

            foreach (var action in actions)
            {
                var actionButton = Instantiate(actionButtonPrefab);
                actionButton.transform.SetParent(actionButtonParent);
                actionButton.Init(unit, action, color);
            }
        }

        public void InitUnitsBar1(List<Unit> units, Color color)
        {
            InitUnitsBar(units, color, unitsBar1);
        }

        public void InitUnitsBar2(List<Unit> units, Color color)
        {
            InitUnitsBar(units, color, unitsBar2);
        }

        void InitUnitsBar(List<Unit> units, Color color, Transform parent)
        {
            parent.DestroyChildren();

            foreach (var unit in units)
            {
                var unitButton = Instantiate(unitButtonPrefab);
                unitButton.transform.SetParent(parent);
                unitButton.Init(unit, color);
            }
        }

        public void ShowFloatingText(Vector3 position, string text, Color color)
        {
            FloatingText instance = Instantiate(floatingTextPrefab);
            instance.transform.SetParent(floatingTextsParent, false);
            instance.SetText(position, text, color);
        }

        public void EndGame(string text, Color color)
        {
            inGameUI.gameObject.SetActive(false);
            endGame.SetActive(true);

            endGameText.text = text;
            endGameText.color = color;
        }
    }
}
