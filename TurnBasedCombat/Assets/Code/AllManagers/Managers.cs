﻿using UnityEngine;
using System.Collections;

namespace TurnBaseCombat.Assets.Code.AllManagers {
    [RequireComponent(typeof(GameManager))]
    [RequireComponent(typeof(UIManager))]
    public class Managers : MonoBehaviour {

        private static GameManager gameManager;
        public static GameManager Game {
            get {
                if (gameManager == null)
                    gameManager = FindObjectOfType<GameManager>();
                return gameManager; }
        }

        private static UIManager uiManager;
        public static UIManager UI
        {
            get { return uiManager; }
        }

        private static TurnManager turnManager;
        internal static TurnManager Turn
        {
            get { return turnManager; }
        }
        
        private static VisualHelpers visualHelpers;
        public static VisualHelpers VisualHelpers
        {
            get
            {
                if (visualHelpers == null)
                    visualHelpers = FindObjectOfType<VisualHelpers>();
                return visualHelpers;
            }
        }


        void Awake() {
            gameManager = GetComponent<GameManager>();
            turnManager = GetComponent<TurnManager>();
            uiManager = GetComponent<UIManager>();
        }
    }
}