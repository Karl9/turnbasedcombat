﻿using System;
using System.Collections.Generic;
using System.Linq;
using TurnBaseCombat.Assets.Code.Units;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.AllManagers
{
    public class TurnManager : MonoBehaviour
    {
        [SerializeField] Player redPlayer;
        [SerializeField] Player bluePlayer;

        public Player currentPlayer;

        public enum TurnState { SelectingAction, ExecutingAction };
        public TurnState state;

        private Action onStartNewTurn;
        public Action OnStartNewTurn
        {
            get { return onStartNewTurn; }
            set { onStartNewTurn = value; }
        }

        public Action<Unit> onSelectedUnit;
        public Unit SelectedUnit
        {
            get { return selectedUnit; }
            set
            {
                if (selectedUnit != null)
                    selectedUnit.CurrentAction.EndAction();
                selectedUnit = value;

                if (selectedUnit != null)
                {
                    if (onSelectedUnit != null)
                        onSelectedUnit(selectedUnit);

                    selectedUnit.CurrentAction.StartAction(selectedUnit);
                    Managers.UI.InitActionsBar(selectedUnit, selectedUnit.actions, selectedUnit.Player.color);
                }


            }
        }

        public Action<Unit> onUnitDeath;

        private Unit selectedUnit;
        private bool gameEnded = false;

        private System.Collections.IEnumerator Start()
        {
            currentPlayer = bluePlayer;

            Managers.UI.InitUnitsBar1(bluePlayer.units, bluePlayer.color);
            Managers.UI.InitUnitsBar2(redPlayer.units, redPlayer.color);

            yield return new WaitForEndOfFrame();
            StartTurn();
        }

        private void StartTurn()
        {
            if (gameEnded)
                return;

            if (onStartNewTurn != null)
                onStartNewTurn();

            currentPlayer.units.First().Select();
        }

        internal void ChangeUnitOrChangeTurn()
        {
            foreach (var unit in currentPlayer.units)
            {
                if (unit.Energy.Energy > 0)
                {
                    unit.Select();
                    return;
                }
            }

            EndTurn();
        }

        public void EndTurn()
        {
            if (currentPlayer == redPlayer)
                currentPlayer = bluePlayer;
            else
                currentPlayer = redPlayer;

            StartTurn();
        }

        public List<Unit> GetAllUnits()
        {
            List<Unit> units = new List<Unit>();
            units.AddRange(bluePlayer.units);
            units.AddRange(redPlayer.units);
            return units;
        }


        private void OnDrawGizmos()
        {
            if (SelectedUnit != null)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawCube(SelectedUnit.transform.position, new Vector3(1, .1f, 1));
            }
        }

        internal void UnithDeath(Unit unit)
        {
            onUnitDeath(unit);

            var player = unit.Player;
            player.units.Remove(unit);

            if (player.units.Count == 0)
                EndGame();

        }

        private void EndGame()
        {
            this.gameEnded = true;

            SelectedUnit = null;
            
            if (bluePlayer.units.Count > 0)
                Managers.UI.EndGame(string.Format("{0} win", bluePlayer.playerName), bluePlayer.color);
            else if (redPlayer.units.Count > 0)
                Managers.UI.EndGame(string.Format("{0} win", redPlayer.playerName), redPlayer.color);
            else
                Managers.UI.EndGame("Draw", Color.black);
        }
    }
}