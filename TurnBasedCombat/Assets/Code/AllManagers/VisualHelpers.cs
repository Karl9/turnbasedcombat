﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.AllManagers
{
    public class VisualHelpers : MonoBehaviour
    {


        public Transform rangeIndicator;

        public LineRenderer lineRenderer;


        public BezierCurve bezier;

        internal void ShowRangeIndicator(Vector3 position, float range)
        {
            rangeIndicator.gameObject.SetActive(true);
            rangeIndicator.transform.position = position;
            var size = range * 2;
            rangeIndicator.localScale = new Vector3(size, rangeIndicator.localScale.y, size);
        }

        public void HideRangeIndicator()
        {
            rangeIndicator.gameObject.SetActive(false);
        }



        public void HideLine()
        {
            lineRenderer.positionCount = 0;
        }

        internal void ShowLine(List<Vector3> positions)
        {
            lineRenderer.positionCount = positions.Count;
            lineRenderer.SetPositions(positions.ToArray());
        }
    }
}
