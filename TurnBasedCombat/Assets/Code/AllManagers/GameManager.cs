﻿using TurnBaseCombat.Assets.Pathfinding;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.AllManagers
{
    public class GameManager : MonoBehaviour
    {
        public TileMap map;

        public Camera mainCamera;

        public Vector3? aimedPoint;
        
    }
}
