﻿using TurnBaseCombat.Assets.Code.AllManagers;
using TurnBaseCombat.Assets.Code.Units;
using UnityEngine;
using UnityEngine.UI;


namespace TurnBaseCombat.Assets.Code.UI.Buttons
{
    [RequireComponent(typeof(Button))]
    public class UnitButton : MonoBehaviour
    {
        Button button;

        [SerializeField] Image selectedImage;
        [SerializeField] Image background;
        [SerializeField] Image unitImage;

        float backgroundAlpha = .25f;

        Unit unit;

        void Start()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(ChangeUnit);
        }

        private void ChangeUnit()
        {
            unit.Select();
        }

        internal void Init(Unit unit, Color color)
        {
            this.unit = unit;
            Managers.Turn.onUnitDeath += OnDeath;
            Managers.Turn.onSelectedUnit += OnSelectedUnit;
            unitImage.sprite = unit.image;
            color.a = backgroundAlpha;
            background.color = color;

        }

        private void OnDeath(Unit unit)
        {
            if (unit == this.unit)
                Destroy(gameObject);
        }

        private void OnSelectedUnit(Unit unit)
        {
            if (this.unit == unit)
            {
                Select();
            } 
            else
            {
                Unselect();
            }
        }

        private void Select()
        {
            if (selectedImage != null)
                selectedImage.gameObject.SetActive(true);
        }

        private void Unselect()
        {
            if (selectedImage != null)
                selectedImage.gameObject.SetActive(false);
        }
    }
}
