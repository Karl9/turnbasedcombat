﻿using System;
using TurnBaseCombat.Assets.Code.Units;
using TurnBaseCombat.Assets.Code.Units.UnitActions;
using UnityEngine;
using UnityEngine.UI;

namespace TurnBaseCombat.Assets.Code.UI.Buttons {
    [RequireComponent(typeof(Button))]
    public class ActionButton : MonoBehaviour {
        Button button;

        [SerializeField] Image background;
        [SerializeField] Image actionImage;

        Unit unit;
        UnitAction action;

        Color backgroundColor;
        public Color activeColor = Color.yellow;

        void Start () {
            button = GetComponent<Button>();
            button.onClick.AddListener(ChangeAction);
        }

        private void ChangeAction()
        {
            unit.CurrentAction = action;
        }

        internal void Init(Unit unit, UnitAction action, Color color)
        {
            this.unit = unit;
            this.action = action;
            unit.onActionChange += OnActionChange;
            actionImage.sprite = action.actionTexture;
            backgroundColor = color;
            background.color = backgroundColor;

            if (unit.CurrentAction == action)
                Select();
        }

        private void OnDestroy()
        {
            if (unit != null)
                unit.onActionChange -= OnActionChange;
        }

        private void OnActionChange(UnitAction action)
        {
            if (this.action == action)
            {
                Select();
            }
            else
            {
                Unselect();
            }
        }

        private void Select()
        {
            actionImage.color = activeColor;
        }

        private void Unselect()
        {
            actionImage.color = Color.white;
        }
    }
}
