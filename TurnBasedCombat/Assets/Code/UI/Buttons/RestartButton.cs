﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace TurnBaseCombat.Assets.Code.UI.Buttons {
    [RequireComponent(typeof(Button))]
    public class RestartButton : MonoBehaviour {
        Button button;

        void Start () {
            button = GetComponent<Button>();
            button.onClick.AddListener(RestartGame);
        }

        private void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
