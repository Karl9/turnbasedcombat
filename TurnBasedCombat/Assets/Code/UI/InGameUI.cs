﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using TurnBaseCombat.Assets.Code.AllManagers;

namespace TurnBaseCombat.Assets.Code.UI
{
    public class InGameUI : MonoBehaviour
    {
        [SerializeField] Text currentPlayerText;

        private void Start()
        {
            Managers.Turn.OnStartNewTurn += OnStartTurn;
        }

        private void OnStartTurn()
        {
            var currentPlayer = Managers.Turn.currentPlayer;
                                
            currentPlayerText.color = currentPlayer.color;
            currentPlayerText.text =  currentPlayer.playerName;
        }
    }
}
