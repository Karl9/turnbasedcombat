﻿using TurnBaseCombat.Assets.Code.Units;
using UnityEngine;
using UnityEngine.UI;

namespace TurnBaseCombat.Assets.Code.UI.Bars {
    public class HealthBar : MonoBehaviour {
        [SerializeField] Image barImage;
        [SerializeField] BarLines barLines;

        int health;
        int maxHealth;

        UnitHealth unit;

        Vector3 offset = new Vector3(0, 1.5f, 0);

        public void Init(UnitHealth unitHealth, int maxHealth, Color color)
        {
            this.health = maxHealth;
            this.maxHealth = maxHealth;
            this.unit = unitHealth;

            unit.OnHealthChanged += UpdateHealth;

            barLines.UpdateBar(100f / maxHealth);
            barImage.color = color;
            ResizeBar();
        }

        private void ResizeBar()
        {
            barImage.fillAmount = health / (float)maxHealth;
        }

        public void UpdateHealth(int health)
        {
            if (health <= 0)
            {
                Destroy(gameObject);
            }
            this.health = health;
            ResizeBar();
        }

        private void Update()
        {
            Vector3 barPos = AllManagers.Managers.Game.mainCamera.WorldToScreenPoint(unit.transform.position + offset);
            transform.position = barPos;
        }
    }
}
