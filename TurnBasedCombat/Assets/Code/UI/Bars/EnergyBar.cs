﻿
using UnityEngine;
using UnityEngine.UI;

namespace TurnBaseCombat.Assets.Code.UI.Bars
{
    public class EnergyBar : MonoBehaviour
    {
        [SerializeField] Image barImage;
        [SerializeField] BarLines barLines;
        
        private void ResizeBar(int energy, int maxEnergy)
        {
            barImage.fillAmount = energy / (float)maxEnergy;
        }

        public void UpdateEnergyBar(int energy, int maxEnergy, Color color)
        {
            barLines.UpdateBar(1f / maxEnergy);
            barImage.color = color;
            ResizeBar(energy, maxEnergy);
        }
    }
}
