﻿using System;
using TurnBaseCombat.Assets.Code.AllManagers;
using TurnBaseCombat.Assets.Pathfinding;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.Units {
    public class UnitHealth : MonoBehaviour {
        int health;
        [SerializeField] int maxHealth;
        public int Health
        {
            get { return health; } 
            private set
            {
                health = value;
                if (onHealthChanged != null)
                    onHealthChanged(health);
            }
        }

        Action<int> onHealthChanged;
        public Action<int> OnHealthChanged
        {
            get
            {
                return onHealthChanged;
            }

            set
            {
                onHealthChanged = value;
            }
        }

        private void Start()
        {
            Health = maxHealth;
            InitHealthBar(maxHealth);
        }

        private void InitHealthBar(int maxHealth)
        {
            var unit = GetComponent<Unit>();
            var color = unit.Player.color;
            Managers.UI.InitHealthBar(this, maxHealth, color);
        }

        public void DoDamage(int damage)
        {
            Managers.UI.ShowFloatingText(transform.position, damage.ToString(), Color.red);
            GetComponent<Unit>().animator.TriggerDamage();
            ChangeHealth(-damage);
        }

        public void Heal(int heal)
        {
            Managers.UI.ShowFloatingText(transform.position, string.Format("+{0}", heal), Color.green);
            ChangeHealth(heal);
        }

        private void ChangeHealth(int value)
        {
            Health += value;
        }

        [ContextMenu("damage")]
        void TestDamage()
        {
            DoDamage(20);
        }
    }
}
