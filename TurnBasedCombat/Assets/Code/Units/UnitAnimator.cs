﻿using UnityEngine;

namespace TurnBaseCombat.Assets.Code.Units {
    [RequireComponent(typeof(Animator))]
    public class UnitAnimator : MonoBehaviour {
        readonly string Attack = "Attack";
        readonly string IsRunning = "IsRunning";
        readonly string Damage = "Damage";
        readonly string Death = "Death";

        Animator animator;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        public void TriggerAttack()
        {
            animator.SetTrigger(Attack);
        }

        public void TriggerDamage()
        {
            animator.SetTrigger(Damage);
        }

        public void TriggerDeath()
        {
            animator.SetTrigger(Death);
        }

        public void SetIsRunning(bool isRunning)
        {
            animator.SetBool(IsRunning, isRunning);
        }

        [ContextMenu("start run")]
        void StartRunning()
        {
            SetIsRunning(true);
        }

        [ContextMenu("end run")]
        void EndRunning()
        {
            SetIsRunning(false);
        }
    }
}
