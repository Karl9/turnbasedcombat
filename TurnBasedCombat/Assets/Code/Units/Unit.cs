﻿using UnityEngine;
using System.Collections.Generic;
using TurnBaseCombat.Assets.Code.AllManagers;
using System.Linq;
using System.Collections;
using System;
using TurnBaseCombat.Assets.Pathfinding;
using TurnBaseCombat.Assets.Code.Units.UnitActions;

namespace TurnBaseCombat.Assets.Code.Units {
    [SelectionBase]
    [RequireComponent(typeof(UnitHealth))]
    [RequireComponent(typeof(UnitEnergy))]
    public class Unit : MonoBehaviour {
        [SerializeField]
        internal Sprite image;

        public TileCoordinates tileCoordinates;
        
        public int moveSpeed = 2;
        
        public List<UnitAction> actions;
        private UnitAction currentAction;

        // DOIT hide in inspector, maby make property
        public UnitAnimator animator;

        private Player player;
        public Player Player
        {
            get { return player; }
        }

        private UnitHealth health;
        public UnitHealth Health
        {
            get
            {
                if (health == null)
                    health = GetComponent<UnitHealth>();
                return health;
            }
        }

        private UnitEnergy energy;
        public UnitEnergy Energy
        {
            get
            {
                if (energy == null)
                    energy = GetComponent<UnitEnergy>();
                return energy;
            }
        }


        public Action<UnitAction> onActionChange;
        public Action onDeath;

        public UnitAction CurrentAction
        {
            get
            {
                if (currentAction == null)
                    CurrentAction = actions.FirstOrDefault();
                return currentAction;
            }
            set
            {
                if (currentAction != null)
                    currentAction.EndAction();

                currentAction = value;
                if (onActionChange != null)
                    onActionChange(currentAction);

                currentAction.StartAction(this);
            }
        }

        private void Awake()
        {
            animator = GetComponentInChildren<UnitAnimator>();
        }

        private void Start()
        {

            var coord = Managers.Game.map.WorldPositionToTileCoord(transform.position);
            tileCoordinates = coord;

            GetComponent<UnitHealth>().OnHealthChanged += OnHealthChanged;
        }

        private void OnHealthChanged(int health)
        {
            if (health <= 0)
            {
                StartCoroutine(DeathCoroutine());
            }
        }

        private IEnumerator DeathCoroutine()
        {
            Managers.Turn.UnithDeath(this);

            animator.TriggerDeath();
            yield return new WaitForSeconds(3f);

            Destroy(gameObject);
        }

        public void Init(Player player)
        {
            this.player = player;
        }

        private void Update()
        {
            if (Managers.Turn.SelectedUnit == this)
            {
                CurrentAction.UpdateAction();
            }
        }

        internal void ClickUnit()
        {
            Managers.Turn.SelectedUnit.CurrentAction.ClickUnit(this);
        }

        public void Select()
        {
            if (Player == Managers.Turn.currentPlayer)
            {
                currentAction = actions[0];

                Managers.Turn.SelectedUnit = this;
                Energy.ShowOnBar();
                Managers.Turn.SelectedUnit.CurrentAction.StartAction(this);
            }
        }

        public IEnumerator RotateTo(Vector3 target)
        {
            //Vector3 targetDir = target.position - transform.position;
            //float step = speed * Time.deltaTime;
            //Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
            //Debug.DrawRay(transform.position, newDir, Color.red);
            //transform.rotation = Quaternion.LookRotation(newDir);

            transform.LookAt(target);

            yield return null;
            
        }
        
    }
}
