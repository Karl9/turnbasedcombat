﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurnBaseCombat.Assets.Code.AllManagers;
using TurnBaseCombat.Assets.Pathfinding;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.Units
{
    public class UnitEnergy : MonoBehaviour
    {
        int energy;
        [SerializeField] int maxEnergy = 2;
        public int Energy
        {
            get { return energy; }
            set
            {
                energy = value;
                ShowOnBar();
            }
        }

        private void Start()
        {
            Managers.Turn.OnStartNewTurn += OnStartNewTurn;
            ResetEnergy();
        }

        private void OnStartNewTurn()
        {
            ResetEnergy();
        }

        void ResetEnergy()
        {
            energy = maxEnergy;
        }

        public void ShowOnBar()
        {
            var unit = GetComponent<Unit>();
            var unitColor = unit.Player.color;
            Managers.UI.ShowEnergy(energy, maxEnergy, unitColor);
        }
    }
}
