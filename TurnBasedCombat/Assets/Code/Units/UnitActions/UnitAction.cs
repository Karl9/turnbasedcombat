﻿using System;
using System.Collections;
using TurnBaseCombat.Assets.Code.AllManagers;
using TurnBaseCombat.Assets.Pathfinding;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.Units.UnitActions
{
    public abstract class UnitAction : MonoBehaviour
    {
        public Sprite actionTexture;
        public int energyCost = 1;

        protected Unit unit;

        private bool isExecutingAction;
        protected bool IsExecutingAction
        {
            get
            {
                return isExecutingAction;
            }

            private set
            {
                isExecutingAction = value;
            }
        }

        public virtual void StartAction(Unit unit)
        {
            this.unit = unit;
        }
        public abstract void ClickUnit(Unit unit);
        public abstract void ClickTile(ClickableTile tile);
        public abstract void EndAction();

        public virtual void OnAimTile(ClickableTile clickableTile) { }
        public virtual void UpdateAction() { }

        public void Execute(IEnumerator routine)
        {
            if (unit.Energy.Energy < energyCost)
            {
                Managers.UI.ShowFloatingText(transform.position, string.Format("No enough energy ({0})", energyCost), Color.white);
                return;
            }

            unit.StartCoroutine(ExecuteCoroutine(routine));
        }

        private IEnumerator ExecuteCoroutine(IEnumerator routine)
        {
            IsExecutingAction = true;
            unit.Energy.Energy -= energyCost;

            yield return routine;
            
            IsExecutingAction = false;

            if (unit.Energy.Energy <= 0)
                Managers.Turn.ChangeUnitOrChangeTurn();
        }
    }
}
