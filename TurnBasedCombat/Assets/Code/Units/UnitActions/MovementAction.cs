﻿using System;
using System.Collections;
using System.Collections.Generic;
using TurnBaseCombat.Assets.Code.AllManagers;
using TurnBaseCombat.Assets.Pathfinding;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.Units.UnitActions
{
    public class MovementAction : UnitAction
    {
        [SerializeField]
        float moveSpeed = 2f;

        private List<Node> currentPath = null;
        private List<Node> spots = new List<Node>();

        TileMap map
        {
            get { return Managers.Game.map; }
        }

        public List<Node> CurrentPath
        {
            get { return currentPath; }

            set
            {
                currentPath = value;
                UpdateLine();
            }
        }

        public override void StartAction(Unit unit)
        {
            base.StartAction(unit);
            
            FindSpots();
        }

        public override void EndAction()
        {
            map.HideSpots();
            Managers.VisualHelpers.HideLine();
        }

        public override void OnAimTile(ClickableTile tile)
        {
            if (IsExecutingAction)
                return;

            CurrentPath = map.GeneratePathTo(unit, tile.tileX, tile.tileZ);
        }

        public void FindSpots()
        {
            spots = map.FindAvailableSpots(unit, unit.moveSpeed);
            map.HideSpots();
            map.SetAvailableSpots(spots);
            map.ShowSpotSelected(unit.tileCoordinates);
        }

        void UpdateLine()
        {
            if (CurrentPath != null)
            {

                int currNode = 0;

                List<Vector3> positions = new List<Vector3>();
                positions.Add(GetPathNodePosition(currNode));
                while (currNode < CurrentPath.Count - 1)
                {
                    Vector3 start = GetPathNodePosition(currNode);
                    Vector3 end = GetPathNodePosition(currNode + 1);

                    positions.Add(end);
                    //Debug.DrawLine(start, end, Color.red);

                    currNode++;
                }

                Managers.VisualHelpers.ShowLine(positions);
            }
        }

        private Vector3 GetPathNodePosition(int currNode)
        {
            return map.TileCoordToWorldCoord(CurrentPath[currNode].x, CurrentPath[currNode].z);
        }

        public override void ClickUnit(Unit unit)
        {
            if (IsExecutingAction)
                return;

            unit.Select();
        }

        public override void ClickTile(ClickableTile tile)
        {
            if (IsExecutingAction)
                return;

            Execute(MoveNextTile());
        }

        public IEnumerator MoveNextTile()
        {
            
            float remainingMovement = unit.moveSpeed;

            while (remainingMovement > 0)
            {
                if (CurrentPath == null)
                    break;

                // Get cost from current tile to next tile
                remainingMovement -= map.CostToEnterTile(CurrentPath[0].x, CurrentPath[0].z, CurrentPath[1].x, CurrentPath[1].z);


                // Move us to the next tile in the sequence
                unit.tileCoordinates = CurrentPath[1].tileCoordinates;

                var nextTilePosition = map.TileCoordToWorldCoord(unit.tileCoordinates.x, unit.tileCoordinates.z); // Update our unity world position
                yield return unit.RotateTo(nextTilePosition);
                yield return RunNextTile(nextTilePosition);

                //yield return new WaitForSeconds(.5f);

                // Remove the old "current" tile
                //if (CurrentPath == null)
                //    break;

                CurrentPath.RemoveAt(0);

                if (CurrentPath.Count == 1)
                {
                    // We only have one tile left in the path, and that tile MUST be our ultimate
                    // destination -- and we are standing on it!
                    // So let's just clear our pathfinding info.
                    CurrentPath = null;
                }
            }

            Managers.VisualHelpers.HideLine();

            FindSpots();
        }

        private IEnumerator RunNextTile(Vector3 nextTilePosition)
        {
            unit.animator.SetIsRunning(true);
            while (transform.position != nextTilePosition)
            {
                var distanceDelta = moveSpeed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, nextTilePosition, distanceDelta);

                yield return null;
            }
            unit.animator.SetIsRunning(false);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(1, 0, 0, .25f);
            foreach (var spot in spots)
            {
                var position = map.TileCoordToWorldCoord(spot.x, spot.z);
                Gizmos.DrawCube(position, Vector3.one);
            }
        }
    }
}
