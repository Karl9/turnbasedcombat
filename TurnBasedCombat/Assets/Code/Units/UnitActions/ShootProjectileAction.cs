﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurnBaseCombat.Assets.Pathfinding;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.Units.UnitActions
{
    public class ShootProjectileAction : UnitAction
    {

        [SerializeField] int damage = 20;
        [SerializeField] GameObject projectile;
        [SerializeField] float projectileSpeed = 7f;


        public override void StartAction(Unit unit)
        {
            base.StartAction(unit);
            ShowTiles();
        }

        private void ShowTiles()
        {
            var map = AllManagers.Managers.Game.map;
            map.HideSpots();

            var units = AllManagers.Managers.Turn.GetAllUnits();
            foreach (var unit in units)
            {
                if (unit != null)
                {
                    map.ShowSpotTarget(unit.tileCoordinates);
                }
            }
            map.ShowSpotSelected(unit.tileCoordinates);
        }

        public override void ClickTile(ClickableTile tile)
        {

        }

        public override void ClickUnit(Unit unit)
        {
            if (unit == this.unit)
                return;

            Execute(DoAction(unit));
        }

        private IEnumerator DoAction(Unit target)
        {
            var targetPosition = target.transform.position;
            yield return unit.RotateTo(targetPosition);

            unit.animator.TriggerAttack();
            yield return new WaitForSeconds(.5f);

            Vector3 relativePos = targetPosition - unit.transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos) * projectile.transform.rotation;
            GameObject go = Instantiate(projectile, unit.transform.position, rotation);
            
            
            while (go.transform.position != targetPosition)
            {
                var distanceDelta = projectileSpeed * Time.deltaTime;
                go.transform.position = Vector3.MoveTowards(go.transform.position, targetPosition, distanceDelta);

                yield return null;
            }

            Debug.LogWarning("hit");
            Destroy(go);
            var health = target.GetComponent<UnitHealth>();
            if (health != null)
                health.DoDamage(damage);

        }
        
        public override void EndAction()
        {

        }

    }
}
