﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurnBaseCombat.Assets.Pathfinding;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.Units.UnitActions
{
    public class HealAction : UnitAction 
    {

        [SerializeField] int heal = 50;
        [SerializeField] GameObject particle;
        [SerializeField] float time = 1.5f;

        public override void StartAction(Unit unit)
        {
            base.StartAction(unit);
            ShowTiles();
        }

        private void ShowTiles()
        {
            var map = AllManagers.Managers.Game.map;
            map.HideSpots();

            var units = AllManagers.Managers.Turn.GetAllUnits();
            foreach (var unit in units)
            {
                if (unit != null)
                {
                    map.ShowSpotTarget(unit.tileCoordinates);
                }
            }
        }

        public override void ClickTile(ClickableTile tile)
        {
        }

        public override void ClickUnit(Unit unit)
        {
            Execute(DoAction(unit));
        }

        private IEnumerator DoAction(Unit target)
        {
            var targetPosition = target.transform.position;

            if (unit != target)
                yield return unit.RotateTo(targetPosition);

            unit.animator.TriggerAttack();
            yield return new WaitForSeconds(.5f);
            
            GameObject go = Instantiate(particle, target.transform.position, Quaternion.identity);
            var health = target.GetComponent<UnitHealth>();
            if (health != null)
                health.Heal(heal);

            yield return new WaitForSeconds(time);

            Destroy(go);
        }

        public override void EndAction()
        {
        }
    }
}
