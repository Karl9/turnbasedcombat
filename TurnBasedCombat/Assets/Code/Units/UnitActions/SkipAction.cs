﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurnBaseCombat.Assets.Pathfinding;

namespace TurnBaseCombat.Assets.Code.Units.UnitActions
{
    public class SkipAction : UnitAction
    {
        public override void StartAction(Unit unit)
        {
            base.StartAction(unit);
            Execute(SkipCoroutine());
        }

        System.Collections.IEnumerator SkipCoroutine()
        {
            yield return null;
            unit.Energy.Energy = 0;
        }

        public override void ClickTile(ClickableTile tile)
        {
            
        }

        public override void ClickUnit(Unit unit)
        {
            
        }

        public override void EndAction()
        {
            
        }

    }
}
