﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurnBaseCombat.Assets.Code.AllManagers;
using TurnBaseCombat.Assets.Pathfinding;
using UnityEngine;

namespace TurnBaseCombat.Assets.Code.Units.UnitActions
{
    public class ThrowAction : UnitAction
    {
        [SerializeField] int damage = 20;
        [SerializeField] float range = 3f;
        [SerializeField] float duration = 1f;

        [SerializeField] Vector3 euler;
        [SerializeField] Vector3 angle1;
        [SerializeField] Vector3 angle2;

        [SerializeField] GameObject projectile;
        [SerializeField] float projectileSpeed = 7f;

        public override void StartAction(Unit unit)
        {
            base.StartAction(unit);

        }

        public override void EndAction()
        {
            Managers.VisualHelpers.HideRangeIndicator();
        }

        public override void UpdateAction()
        {
            if (IsExecutingAction)
                return;

            if (Managers.Game.aimedPoint.HasValue)
            {
                var position = Managers.Game.aimedPoint.Value;
                Managers.VisualHelpers.ShowRangeIndicator(position, range);

                List<Vector3> arc = GetArc(transform.position, position);
                Managers.VisualHelpers.ShowLine(arc);

                ShowTiles(arc.Last());

                if (Input.GetMouseButtonUp(0))
                {
                    if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                        return;

                    Execute(ThrowCoroutine(arc));
                }
            }
        }

        private void ShowTiles(Vector3 target)
        {
            var map = Managers.Game.map;
            map.HideSpots();
            map.ShowSpotSelected(unit.tileCoordinates);

            var overlap = Physics.OverlapSphere(target, range);
            foreach (var o in overlap)
            {
                var unit = o.GetComponent<Unit>();
                if (unit != null)
                {
                    map.ShowSpotTarget(unit.tileCoordinates);
                }
            }
            
        }

        private IEnumerator ThrowCoroutine(List<Vector3> arc)
        {
            GameObject go = Instantiate(projectile, unit.transform.position, Quaternion.identity);

            yield return unit.RotateTo(arc.Last());
            unit.animator.TriggerAttack();

            yield return new WaitForSeconds(1.4f);

            for (int i = 0; i < arc.Count - 1; i++)
            {
                var start = arc[i];
                var end = arc[i + 1];

                while (go.transform.position != end)
                {
                    var distanceDelta = projectileSpeed * Time.deltaTime;
                    go.transform.position = Vector3.MoveTowards(go.transform.position, end, distanceDelta);

                    yield return null;
                }
            }
            Debug.LogError("hit");

            var colliders = Physics.OverlapSphere(arc.Last(), range);
            foreach (var c in colliders)
            {
                var health = c.GetComponent<UnitHealth>();
                if (health != null)
                    health.DoDamage(damage);
            }

            yield return new WaitForSeconds(.5f);
            Destroy(go);
        }

        List<Vector3> GetArc(Vector3 start, Vector3 end)
        {
            var bezier = Managers.VisualHelpers.bezier;
            var p1 = bezier[0];
            var p2 = bezier[1];
            p1.position = start;
            p2.position = end;

            List<Vector3> points = new List<Vector3>();
            var count = 20;
            for (int i = 0; i <= count; i++)
            {
                var t = i / (float)count;
                var position = bezier.GetPointAt(t);
                points.Add(position);
            }


            
            return points;
        }

        public override void ClickUnit(Unit unit) { }
        public override void ClickTile(ClickableTile tile) { }

    }
}
