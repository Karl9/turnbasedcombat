﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurnBaseCombat.Assets.Pathfinding;
using System.Collections;
using UnityEngine;
using TurnBaseCombat.Assets.Code.AllManagers;

namespace TurnBaseCombat.Assets.Code.Units.UnitActions
{
    public class AttackAction : UnitAction
    {
        [SerializeField] int damage = 20;
        [SerializeField] float range = 3f;
        [SerializeField] float duration = 1f;

        
        public override void StartAction(Unit unit)
        {
            base.StartAction(unit);
            Managers.VisualHelpers.ShowRangeIndicator(transform.position, range);
            ShowTiles();
        }

        private void ShowTiles()
        {
            var map = Managers.Game.map;
            map.HideSpots();
            
            var overlap = Physics.OverlapSphere(transform.position, range);
            foreach (var o in overlap)
            {
                var unit = o.GetComponent<Unit>();
                if (unit != null)
                {
                    map.ShowSpotTarget(unit.tileCoordinates);
                }
            }
            map.ShowSpotSelected(unit.tileCoordinates);
        }

        public override void EndAction()
        {
            Managers.VisualHelpers.HideRangeIndicator();
        }

        public override void ClickTile(ClickableTile tile)
        {
            if (IsExecutingAction)
                return;
        }

        public override void ClickUnit(Unit unit)
        {
            if (IsExecutingAction)
                return;

            if (CanAttack(unit))
                Execute(AttackCoroutine(unit));
            else
                Debug.Log("Cant attack: not in range");
        }

        private bool CanAttack(Unit unit)
        {
            if (unit == this.unit)
                return false;

            if (Vector3.Distance(this.unit.transform.position, unit.transform.position) < range)
                return true;
            return false;
        }

        private IEnumerator AttackCoroutine(Unit unit)
        {

            var unitHealth = unit.GetComponent<UnitHealth>();
            yield return this.unit.RotateTo(unit.transform.position);
            unitHealth.DoDamage(20);
            this.unit.animator.TriggerAttack();

            yield return new WaitForSeconds(duration);
            
        }

        

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position, range);
        }
    }
}
